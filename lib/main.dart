import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("text field input type datetime"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'input datetime',
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              padding: EdgeInsets.symmetric(horizontal: 8),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 1)),
              child: TextField(
                controller: controller,
                decoration: InputDecoration(
                  hintText: "Nhập ngày/tháng/năm",
                  border: InputBorder.none,
                ),
                keyboardType: TextInputType.number,
                //inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                onChanged: (value) {
                  print("$value");
                  controller
                    ..text = checkInput(value)
                    ..selection =
                        TextSelection.collapsed(offset: controller.text.length);
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  checkInput(String value) {
    setState(() {
      print("length: ${value.length}");
      if (value.length == 1 && int.parse(value) > 3) {
        value = "0$value";
        controller.text = value;
      }
      if (value.length == 2) {
        try {
          int time = int.parse(value);
          if (time > 31) {
            value = "0${value.substring(0, 1)}/${value.substring(1, 2)}";
            controller.text = value;
          } else {
            value = "$value";
            controller.text = "$value";
          }
        } catch (error) {}
      }
      if (value.length > 2) {
        if (value.length == 4 || value.length == 5) {
          int time;
          try {
            if (value.length == 4)
              if(value.contains('/')){
                time=int.parse(value.substring(value.indexOf('/')+1, value.length));
                print("check option 1");
              }
              else {
                time = int.parse(value.substring(2, value.length));
                print("check option 2 ");
              }
          } catch (error) {
            if (value.length == 5) time = int.parse(value.split("/")[1]);
          }
          if (time != null && time>0) {
            if (time>1 && time < 10) {
              value = "${controller.text.substring(0, 2)}/0$time";
              controller.text = "$value";
            } else if (time <= 12) {
              value = "${controller.text.substring(0, 2)}/$time";
              controller.text = "$value";
            } else {
              value =
                  "${controller.text.substring(0, 2)}/0${time~/10}/${time%10}";
              controller.text = "$value";
              print("check option 5 $time");
            }
          }
        }
        else if (value.length > 5 && value.length <= 10) {
          if (controller.text.split('/').length == 3) {
            value =
                "${controller.text.substring(0, 5)}/${controller.text.split('/')[controller.text.split('/').length - 1]}";
          } else {
            value =
                "${controller.text.substring(0, 5)}/${controller.text.substring(5, controller.text.length)}";
          }
          controller.text = "$value";
        } else if (value.length > 10) {
          value = value.substring(0, 10);
          controller.text = "$value";
        }
      }
    });
  }
}
